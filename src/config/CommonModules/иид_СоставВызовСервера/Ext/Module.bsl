﻿// Состав транзакций, серверный контекст
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Добавляет запись об объекте транзакции
//
// Параметры: 
// 	Транзакция - Строка - Идентификатор транзакции
// 	Ссылка - ЛюбаяСсылка - Ссылка на объект
// 	Идентификатор - Строка - Условный идентификатор программного объекта записи
// 	ВерсияНомер - Число - Номер версии объекта в транзакции
// 	ТипИмя - Строка - Полное имя типа
// 	ЗаписьРежим - Перечисление.иид_РежимыЗаписи - Режим записи
// 	ПроведениеРежим - Перечисление.иид_РежимыПроведения - Режим проведения
// 	ИзменениеПометкиУдаления - Перечисление.иид_ИзменениеБулево - Режим изменения пометки удаления
// 	ДатаВремя - Дата - Время начала записи объекта
// 	МоментВремени - Число - Время начала записи объекта числом
//
// BSLLS:NumberOfParams-off - структура в данном случае менее наглядна
Процедура ОбъектДобавить(Транзакция
	, Ссылка
	, Идентификатор
	, ВерсияНомер
	, ТипИмя
	, ЗаписьРежим
	, ПроведениеРежим
	, ИзменениеПометкиУдаления
	, ДатаВремя
	, МоментВремени) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	Запись	= РегистрыСведений.иид_СоставТранзакцийОбъекты.СоздатьМенеджерЗаписи();
	ЗаписьЗаполнить(Запись, ДатаВремя, МоментВремени);
	Запись.Транзакция				= Транзакция;
	Запись.Объект					= Ссылка;
	Запись.Идентификатор			= Идентификатор;
	Запись.НомерВерсии				= ВерсияНомер;
	Запись.ТипИмя					= ТипИмя;
	Запись.РежимЗаписи				= ЗаписьРежим;
	Запись.РежимПроведения			= ПроведениеРежим;
	Запись.ИзменениеПометкиУдаления	= ИзменениеПометкиУдаления;
	
	иид_Модуль.ом_ОбъектПрикладной().ДанныеЗаписать(Запись, Истина);
	
// BSLLS:NumberOfParams-on
КонецПроцедуры // ОбъектДобавить 

// Добавляет запись о регистре транзакции
//
// Параметры: 
// 	Транзакция - Строка - Идентификатор транзакции
// 	Регистратор - ДокументСсылка - Регистратор
// 	Идентификатор - Строка - Условный идентификатор программного объекта записи
// 	ВерсияНомер - Число - Номер версии объекта в транзакции
// 	ТипИмя - Строка - Полное имя типа регистра
// 	Замещение - Булево - Флаг замещения
// 	Отбор - Строка - Сериализованный отбор для независимых регистров
// 	ДатаВремя - Дата - Время начала записи объекта
// 	МоментВремени - Число - Время начала записи объекта числом
//
// BSLLS:NumberOfParams-off
Процедура РегистрДобавить(Транзакция
	, Регистратор
	, Идентификатор
	, ВерсияНомер
	, ТипИмя
	, Замещение
	, Отбор
	, ДатаВремя
	, МоментВремени) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	Запись	= РегистрыСведений.иид_СоставТранзакцийРегистры.СоздатьМенеджерЗаписи();
	ЗаписьЗаполнить(Запись, ДатаВремя, МоментВремени);
	Запись.Транзакция		= Транзакция;
	Запись.Объект			= Регистратор;
	Запись.Идентификатор	= Идентификатор;
	Запись.НомерВерсии		= ВерсияНомер;
	Запись.ТипИмя			= ТипИмя;
	Запись.Замещение		= Замещение;
	Запись.Отбор			= иид_РегистрОтбор.СтруктураВХранилище(Отбор);
	
	иид_Модуль.ом_ОбъектПрикладной().ДанныеЗаписать(Запись, Истина);
	
// BSLLS:NumberOfParams-on
КонецПроцедуры // РегистрДобавить 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Заполняет общие поля записей регистров состава
//
// Параметры: 
// 	Запись - РегистрСведенийМенеджерЗаписей - Запись регистра
// 	ДатаВремя - Дата - Время начала записи объекта
// 	МоментВремени - Число - Время начала записи объекта числом
//
Процедура ЗаписьЗаполнить(Запись, ДатаВремя, МоментВремени)
	
	Запись.ДатаВремя		= ДатаВремя;
	Запись.МоментВремени	= МоментВремени;
	
КонецПроцедуры // ЗаписьЗаполнить 

#КонецОбласти
