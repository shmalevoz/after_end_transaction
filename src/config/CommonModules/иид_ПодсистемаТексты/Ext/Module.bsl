﻿// Отображаемые тексты подситсемы
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает текст лога создания фонового процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off - все верно
Функция ВзаимоблокировкаСоздатьЛогЗапущена(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Запущена фоновая взаимоблокировка для транзакции %1'", ЯзыкКод);
	
// BSLLS:Typo-on
КонецФункции // ВзаимоблокировкаСоздатьЛогЗапущена 

// Возвращает текст лога начала выполненя фонового процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off
Функция ВзаимоблокировкаПроцессЛогНачат(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Начат фоновый процесс взаимоблокировки транзакции %1'", ЯзыкКод);
	
// BSLLS:Typo-on
КонецФункции // ВзаимоблокировкаПроцессЛогНачат 

// Возвращает текст лога успешного завершения процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off
Функция ВзаимоблокировкаПроцессЛогЗавершениеУспех(ЯзыкКод = "") Экспорт
	
	Возврат ВзаимоблокировкаПроцессЛогЗавершение(ЯзыкКод)
	+ НСтр("ru = 'успешно'", ЯзыкКод);
	
// BSLLS:Typo-on
КонецФункции // ВзаимоблокировкаПроцессЛогЗавершениеУспех 

// Возвращает текст лога НЕуспешного завершения процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off
Функция ВзаимоблокировкаПроцессЛогЗавершениеОшибка(ЯзыкКод = "") Экспорт
	
	Возврат ВзаимоблокировкаПроцессЛогЗавершение(ЯзыкКод)
	+ НСтр("ru = 'с ошибкой'", ЯзыкКод);
	
// BSLLS:Typo-on
КонецФункции // ВзаимоблокировкаПроцессЛогЗавершениеОшибка 

// Возвращает текст вызываемого исключения процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off
Функция ВзаимоблокировкаПроцессИсключение(ЯзыкКод = "") Экспорт
	
	// BSLLS:Typo-on

	Возврат НСтр("ru = 'Таймаут ожидания блокировки записи'", ЯзыкКод);
	
КонецФункции // ВзаимоблокировкаПроцессИсключение 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Общий текст лога завершения процесса взаимоблокировки
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
// BSLLS:Typo-off
Функция ВзаимоблокировкаПроцессЛогЗавершение(ЯзыкКод = "")
	
	// BSLLS:Typo-on
	
	Возврат НСтр("ru = 'Фоновая блокировка транзакции %1 завершена '", ЯзыкКод);
	
КонецФункции // ВзаимоблокировкаПроцессЛогЗавершение 

// Возвращает текст лога служебного объекта
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ПодпискаЛогОбъектСлужебный(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Это служебный объект, имя типа %1'", ЯзыкКод);
	
КонецФункции // ПодпискаЛогОбъектСлужебный 

// Возвращает текст лога начала процесса ожидания завершения транзакции
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТранзакцияЛогОжиданиеПроцессНачат(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Начат фоновый процесс ожидания завершения транзакции %1'", ЯзыкКод);
	
КонецФункции // ТранзакцияЛогОжиданиеПроцессНачат 

// Возвращает текст лога итерации процесса ожидания завершения транзакции
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТранзакцияЛогОжиданиеПроцессИтерация(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Ожидание завершения транзакции %1, %2 секунд'", ЯзыкКод);
	
КонецФункции // ТранзакцияЛогОжиданиеПроцессИтерация 

// Возвращает текст лога завершения процесса ожидания завершения транзакции
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТранзакцияЛогОжиданиеПроцессЗавершение(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Ожидание завершения транзакции %1 завершилось со статусом %2'", ЯзыкКод);
	
КонецФункции // ТранзакцияЛогОжиданиеПроцессЗавершение 

#КонецОбласти
