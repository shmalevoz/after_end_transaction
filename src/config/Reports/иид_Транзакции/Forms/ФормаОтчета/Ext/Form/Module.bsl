﻿
#Область ОписаниеПеременных

#КонецОбласти

#Область ОбработчикиСобытийФормы

#КонецОбласти

#Область ОбработчикиКомандФормы

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовШапкиФормы

// По расшифровке отчета. Открываем объект/набор записей
// 
// BSLLS:MissingParameterDescription-off - см. справку
&НаКлиенте
Процедура РезультатОбработкаРасшифровки(Элемент, Расшифровка, СтандартнаяОбработка)
	// BSLLS:MissingParameterDescription-on
	
	СтандартнаяОбработка	= Ложь;
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
